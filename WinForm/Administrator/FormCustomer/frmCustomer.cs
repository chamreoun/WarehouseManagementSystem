﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using BusinessLayer.CustomerData;
using WinForm.Administrator;
using AttributeLayer;
using WinMessage;



namespace WinForm.Administrator
{
    public partial class frmCustomer : DevComponents.DotNetBar.Office2007Form
    {
        public frmCustomer()
        {
            InitializeComponent();
        }

        private void frmCustomer_Load(object sender, EventArgs e)
        {

        }
        

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
                OperationCustomer cus = new OperationCustomer();
                cus.ID = txtId.Text;
                cus.Name = txtName.Text;
                cus.Sex = cboSex.SelectedItem.ToString();
                cus.Phone = txtPhone.Text;
                cus.Email = txtEmail.Text;
                cus.Note = txtNote.Text;
                cus.Address = txtAddress.Text;
                try
                {
                    cus.save();

                } catch (Exception E)
                    {
                     MyMessage.showMessage(E + " ");
                    }
          }

      
        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            frmCustomerList frm = new frmCustomerList();
            frm.ShowDialog();
            
            if (frm.SelectCustomer != null)
            {
              
         
            }
        }

    }
}
