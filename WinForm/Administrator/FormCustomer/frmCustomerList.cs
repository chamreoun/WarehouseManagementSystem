﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLayer.CustomerData;
using AttributeLayer;
using WinMessage;
using WinForm.Administrator;
using System.Data.SqlClient;
using System.Data;
using DatabaseLayer;
using System.Data;

namespace WinForm.Administrator
{
    public partial class frmCustomerList : DevComponents.DotNetBar.Office2007Form
    {
        public frmCustomerList()
        {
            InitializeComponent();
        }
        public CustomerAttribute customerAttribute;
        public GetCustomer gcm;
        public CustomerAttribute SelectCustomer
        {
            get { return customerAttribute; }
        }

        private void frmCustomerList_Load(object sender, EventArgs e)
        {
            List<CustomerAttribute> listCustomer = new List<CustomerAttribute>();

            try
            {
                gcm = new GetCustomer();
                DataTable dt = gcm.listAllCustomer;
                dataGridView1.DataSource = dt;
                dt.Dispose();
                //customerAttribute = new CustomerAttribute();
                //DataTable dt = gcm.listCustomer;
               
                    
                //foreach (DataRow row in dt.Rows)
                //{
                //    customerAttribute.ID = row["Id"].ToString();
                //    customerAttribute.Name = row["Name"].ToString();
                //    customerAttribute.Sex = row["Sex"].ToString();
                //    customerAttribute.Phone = row["Phone"].ToString();
                //    customerAttribute.Email = row["Email"].ToString();
                //    customerAttribute.Note = row["Note"].ToString();
                //    listCustomer.Add(customerAttribute);
                //}
               
                //    dataGridView1.DataSource = listCustomer;
            }
            catch (Exception E)
            {
                MyMessage.showMessage("frmCustomerLoad  : " + E);
            }

        }
        private void toolStripButton1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }

} 

