﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using AttributeLayer;
using WinMessage;

namespace DatabaseLayer.CustomerData
{
    public class GetCustomerModel
    {
        public CustomerAttribute cus;
        public DataTable getAll()
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM dbo.showAllCustomer", Connection.getConnection());
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt ;
            }
            catch (Exception E)
            {
                MyMessage.showMessage("DatabaesLayer getCustomer " + E);
                return null;

            }
            
        }

        public DataTable getCustomerById(String id)
        {
            try
            {
                String sqlFun = "SELECT * FROM dbo.getCustomerById("+id+")";
                SqlDataAdapter da =new SqlDataAdapter(sqlFun,Connection.getConnection());
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }catch(Exception E)
            {
                MyMessage.showMessage("GetCustomerModel GetCustomer By Id : " + E);
                return null;
            }
        }



     



    }
}
