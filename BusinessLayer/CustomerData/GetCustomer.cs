﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentationLayer.ICustomerData;
using DatabaseLayer.CustomerData;
using AttributeLayer;
using System.Data;
using System.Data.SqlClient;
using AttributeLayer;

namespace BusinessLayer.CustomerData
{
    public class GetCustomer : IGetCustomer
    {
        DataTable dt;
        GetCustomerModel gcm;
        public  DataTable listAllCustomer
        {
            get
            {
                dt = new DataTable();
                 gcm = new GetCustomerModel();
                dt = gcm.getAll();
                return dt;
            }
        }


        public DataTable showCustomerById(string id)
        {
            DataTable dt = new DataTable();
            gcm = new GetCustomerModel();
            dt = gcm.getCustomerById(id);
            return dt;
        }
    }
}
